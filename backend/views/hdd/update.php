<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hdd */

$this->title = 'Update Hdd: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hdds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hdd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
