<?php
namespace backend\controllers;

use app\models\HddSearch;
use common\components\exceptions\ApiException;
use common\models\HddForm;
use yii\base\Exception;
use yii\web\UploadedFile;

class SendController extends ApiController
{

    /**
     * @return array
     * @throws Exception
     */
    public function actionHdd(){
        $model = new HddForm();

        $model->imageFile = UploadedFile::getInstance($model, 'photo');

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {

            return ['status' => 'success'];

        } else {
            throw new ApiException($model->getErrors());
        }
    }

}