<?php
namespace backend\controllers;

use common\components\exceptions\ApiException;
use common\models\LoginForm;
use yii\base\Exception;

class AuthController extends ApiController
{

    /**
     * @return array
     * @throws Exception
     */
    public function actionIndex(){
        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            $model->_user->auth_token = sha1(time().$model->username);
            if($model->_user->save()){
               return ['token' => $model->_user->auth_token];
            }
            throw new ApiException($model->_user->getErrors());
        } else {
            throw new ApiException($model->getErrors());
        }
    }

}