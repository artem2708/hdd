<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Hdd;

/**
 * HddSearch represents the model behind the search form of `common\models\Hdd`.
 */
class HddSearch extends Hdd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_time', 'model', 'sn', 'pn', 'fw', 'photo', 'comment', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hdd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_time' => $this->created_time,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'pn', $this->pn])
            ->andFilterWhere(['like', 'fw', $this->fw])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
